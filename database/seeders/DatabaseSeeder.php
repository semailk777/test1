<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (User::query()->where('email', 'admin@mail.ru')->get()->isEmpty()){
            User::query()->create([
                'name' => 'Admin',
                'password' => Hash::make('adminadmin'),
                'email' => 'admin@mail.ru'
            ]);
        }

        Product::factory()->count(200)->create();
    }
}
