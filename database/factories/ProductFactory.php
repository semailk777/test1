<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $status = ["available", "unavailable"];

        return [
            'name' => $this->faker->name,
            'status' => $status[random_int(0, 1)],
            'data' => [ 'color' => $this->faker->colorName, 'country' => $this->faker->country],
            'article' => Str::random(10)
        ];
    }
}
