<h1 style="text-align: center; color: red">Тестовое задание</h1>

1. Переносим к себе репозиторий.
2. Пишем команду "docker-compose up -d" в корне проекта, для поднятия рабочего окружения.
3. После того как окружение будет поднято, заходим внутрь контейнера "docker-compose exec app bash".
4. Подтягиваем все зависимости "composer update".
5. Создаем ключ для проекта "php artisan key:generate".
6. Запускаем миграции и сиды "php artisan migrate --seed".
7. Стучимся в браузере на адрес "localhost:8000/login".
8. Email: <span style="color:blue">admin@mail.ru</span> password: <span style="color:blue"> adminadmin </span>. Это данные для входа нашего админа .
