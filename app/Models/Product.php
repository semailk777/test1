<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $status
 * @property string $name
 * @property string $article
 * @property array $data
 */
class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
      'article',
      'data',
      'name',
      'status'
    ];

    protected $casts = [
      'data' => 'array'
    ];

    /**
     * @param $query Builder
     * @return Builder
     */
    public function scopeAvailable(Builder $query): Builder
    {
        return $query->where('status', 'available');
    }
}
