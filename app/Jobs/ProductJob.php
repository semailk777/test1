<?php

namespace App\Jobs;

use App\Models\Product;
use App\Models\User;
use App\Notifications\ProductCreateNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProductJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $productId;
    private $adminId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($productId, $adminId)
    {
        //
        $this->productId = $productId;
        $this->adminId = $adminId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $admin = User::find($this->adminId);
        $product = Product::find($this->productId);

        $admin->notify(new ProductCreateNotification([
            'id' => $product->id,
            'name' => $product->name,
            'article' => $product->article,
            'status' => $product->status,
        ]));
    }
}
