<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Jobs\ProductJob;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;

class ProductController extends Controller
{

    /** @return View */
    public function index(): View
    {
        $products = Product::query()
            ->orderByDesc('created_at')
            ->paginate(15);

        return view('welcome', [
            'products' => $products
        ]);
    }

    /**
     * @param ProductRequest $request
     * @return RedirectResponse
     */
    public function store(ProductRequest $request): RedirectResponse
    {
        $attributes = $this->getDataRequest($request);
        $admin = User::query()->where('email', config('products.role.email'))->first();

        $product = new Product();
        $product->name = $request->name;
        $product->article = $request->article;
        $product->status = $request->status;
        $product->data = $attributes;
        $product->save();

        ProductJob::dispatch($product->id, $admin->id);

        return redirect()->back()->with(['success' => 'Продукт добавлен']);
    }

    /**
     * @param Product $product
     * @return mixed
     */
    public function show(Product $product)
    {
        if (Gate::check('isAdmin', auth()->user())) {
            return \view('edit', [
                'product' => $product
            ]);
        }
        return redirect()->route('welcome');
    }

    /**
     * @param ProductUpdateRequest $request
     * @param Product $product
     * @return RedirectResponse
     */
    public function update(ProductUpdateRequest $request, Product $product): RedirectResponse
    {
        $attributes = $this->getDataRequest($request);

        $product->update([
            'name' => $request->name,
            'article' => $request->article,
            'status' => $request->status,
            'data' => $attributes
        ]);

        return redirect()->back()->with([
            'success' => 'successfully updated'
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getDataRequest(Request $request): array
    {
        if (Gate::check('isAdmin', auth()->user())) {
            $attributes = [];
            foreach ($request->attributes_name as $key => $name) {
                $attributes[$name] = $request->attributes_value[$key];
            };
            return $attributes;
        }
        return [];
    }

    /**
     * @param Product $product
     * @return RedirectResponse
     */
    public function delete(Product $product): RedirectResponse
    {
        if (Gate::check('isAdmin', auth()->user())) {
            $product->delete();

            return redirect()->route('welcome');
        }
        return redirect()->route('welcome');
    }
}
