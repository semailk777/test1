@extends('layouts.main')
@section('content')
    <div class="content">
        <header>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <span>{{ auth()->user()->name }}</span>
        </header>
        {{ $products->links() }}
        <div class="tableAndAttribute">
            <table>
                <tr>
                    <th>АРТИКУЛ</th>
                    <th>НАЗВАНИЕ</th>
                    <th>СТАТУС</th>
                    <th>АТРИБУТЫ</th>
                </tr>
                @foreach($products as $product)
                    <tr class="table-content">
                        <td><a class="text-cyan-900" href="{{ route('product.show', $product->id) }}">{{ $product->article }}</a> </td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->status }}</td>
                        <td>
                            @foreach($product->data as $key => $attr)
                                {{ $key }} : {{ $attr }}
                            @endforeach
                        </td>
                    </tr>
                @endforeach

            </table>
            @can('isAdmin', auth()->user())
                <button type="button"
                        class="px-4 py-1 text-sm text-lime-600 font-semibold rounded-full border border-green-200 hover:text-white hover:bg-lime-600 hover:border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2 add">
                    Добавить
                </button>
            @endcan
            <div class="modal">
                <div class="x">X</div>
                <div class="modal-title">
                    Добавить продукт
                </div>
                <form action="{{ route('product.store') }}" method="post">
                    @csrf
                    <div class="modal-inputs">
                        <label for="article">Артикул</label>
                        <input value="{{ old('article') }}" required class="text-black" type="text" id="article" name="article">
                        <label for="name">Название</label>
                        <input value="{{ old('name') }}" required class="text-black" type="text" id="name" name="name">
                        <label for="status">Статус</label>
                        <select name="status" id="status">
                            <option value="available">Доступен</option>
                            <option value="unavailable">Не доступен</option>
                        </select>
                    </div>
                    <div class="add-attribute">
                        <label for="attribute">Атрибуты</label>
                        <span id="attribute">+ Добавить атрибут</span>
                    </div>
                    <div class="new-attribute"></div>

                    <br>
                    <button
                        class="px-4 py-1 text-sm text-lime-600 font-semibold rounded-full border border-green-200 hover:text-white hover:bg-lime-600 hover:border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2  submit"
                        type="submit">Добавить
                    </button>
                </form>
            </div>
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div style="color: red">{{$error}}</div>
                @endforeach
            @endif
            @if (session('success'))
                <div style="color: green">{{session('success')}}</div>
            @endif
        </div>
    </div>
@endsection
