<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com/"></script>
    <link rel="stylesheet" href="css/style.css">

    <title>Document</title>
</head>
<body>
<div class="main">
    <div class="sidebar">
        <div class="logo-content">
            <div id="logo">
                PI<span>N</span>
            </div>
            <div id="logo-title">
                <p>Enterprise Resource Planning</p>
            </div>
        </div>

        <div class="links">
            <a href="{{ route('welcome') }}">Продукты</a>
        </div>
    </div>

    @yield('content')
</div>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.add').click(function () {
            $('.modal').show();
        });

        $('.x').click(function () {
            $('.modal').hide();
        });

        $('#attribute').click(function () {
            $('.new-attribute').append(
                '<div class="input-div">' +
                '<div class="attributes_name">' +
                '<label for="attributes_name">Название</label>' +
                '<input class="text-black" required id="attributes_name" name="attributes_name[]" type="text">' +
                '</div>' +
                '<div class="attributes_value">' +
                '<label for="attributes_value">Значение</label>' +
                '<input class="text-black" required id="attributes_value" name="attributes_value[]" type="text">' +
                '</div>' +
                '<div class="delete">' +
                '<img  src="https://img.icons8.com/fluency/24/000000/filled-trash.png"/>' +
                '</div>' +
                '</div>'
            )
            $('.delete').click(function () {
                $(this).parent().remove();
            });
        });
    })
</script>

</body>
</html>
