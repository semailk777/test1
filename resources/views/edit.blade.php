@extends('layouts.main')
@section('content')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/edit.css') }}">
    <div class="content">
        <header>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <a href="#">Продукты</a>
            <span>{{ auth()->user()->name }}</span>
        </header>

        <div class="">
            <form action="{{route( 'product.delete', $product->id) }}" method="post">
                @csrf
                <button type="submit" onclick="return confirm('Удалить?')"
                        class="px-12 py-5 text-sm text-red-600 font-semibold rounded-full border border-green-200 hover:text-white hover:bg-red-600 hover:border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2 destroy">
                    Удалить
                </button>
            </form>
            <form action="{{ route('product.update', $product->id) }}" method="post">
                @csrf
                <div class="modal-inputs">
                    <label for="article">Артикул</label>
                    <input value="{{ $product->article }}" required class="text-black" type="text" id="article"
                           name="article">
                    <label for="name">Название</label>
                    <input value="{{ $product->name }}" required class="text-black" type="text" id="name" name="name">
                    <label for="status">Статус</label>
                    <select name="status" id="status">
                        <option @if($product->status === 'available') selected @endif value="available">Доступен
                        </option>
                        <option @if($product->status === 'unavailable') selected @endif value="unavailable">Не
                            доступен
                        </option>
                    </select>

                </div>

                <h1 class="m-5" style="font-size: 25px">Атрибуты</h1>
                <div class="new-attribute">
                    @foreach($product->data as $key => $attr)
                        <div class="input-div">
                            <div class="attributes_name">
                                <label for="attributes_name">Название</label>
                                <input value="{{ $key }}" class="text-black" required id="attributes_name"
                                       name="attributes_name[]"
                                       type="text">
                            </div>
                            <div class="attributes_value">
                                <label for="attributes_value">Значение</label>
                                <input value="{{ $attr }}" class="text-black" required id="attributes_value"
                                       name="attributes_value[]"
                                       type="text">
                            </div>
                            <div class="delete">
                                <img src="https://img.icons8.com/fluency/24/000000/filled-trash.png"/>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="add-attribute">
                    <span id="attribute" class="text-cyan-500">+ Добавить атрибут</span>
                </div>

                <br>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div style="color: red">{{$error}}</div>
                    @endforeach
                @endif
                @if (session('success'))
                    <div style="color: green">{{session('success')}}</div>
                @endif
                <button
                    class="ml-5 px-4 py-1 text-sm text-lime-600 font-semibold rounded-full border border-green-200 hover:text-white hover:bg-lime-600 hover:border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2  submit"
                    type="submit">Обновить
                </button>
            </form>
        </div>
    </div>
@endsection
